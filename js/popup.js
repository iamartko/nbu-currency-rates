jQuery(document).ready(function () {
    var dateEl = jQuery('#date');
    var button = jQuery('#show');
    var CurrentDate = new currentDate();

    dateEl.val(CurrentDate.getDate());
    dateEl.attr('max', CurrentDate.getDate());

    jQuery(button).click(function () {
        showRate();
    });
});

function currentDate() {
    var currentDate, today = new Date(), year = today.getFullYear(), month = today.getMonth() + 1, day = today.getDate();
    month = month < 10 ? '0' + month : month;
    day = day < 10 ? '0' + day : day;
    this.year = year;
    this.month = month;
    this.day = day;
    this.getDate = function () {
        return this.year + '-' + this.month + '-' + this.day;
    }
}

function formatDateForRequest(date) {
    return date.replace(/-/g, '');
}

function showRate() {
    var dateEl = jQuery('#date');
    var currencyEl = jQuery('#currency');
    var button = jQuery('#show');
    var loader = jQuery('#loader');
    var rateEl = jQuery('#rate');
    var CurrentDate = new currentDate();
    var date = dateEl.val(), currency = currencyEl.val();

    if (date && currency && date <= CurrentDate.getDate()) {
        button.attr('disabled', 'disabled');
        dateEl.removeClass('error');
        currencyEl.removeClass('error');
        loader.show();
        rateEl.hide();
        var formattedDate = formatDateForRequest(date);
        var url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=' + currency + '&date=' + formattedDate + '&json';
        console.log(url);

        jQuery.ajax({
            url: url,
            success: function (result) {
                button.removeAttr('disabled');
                loader.hide();
                if (result[0] && result[0]['rate']) {
                    var rate = result[0]['rate'];
                    rateEl.html('1 ' + currency + ' = ' + rate + ' UAH');
                    rateEl.show();
                }
            }
        });
    }
    else {
        if (!date || date > CurrentDate.getDate()) {
            dateEl.addClass('error');
        }
        else if (!currency) {
            currencyEl.addClass('error');
        }
    }
}