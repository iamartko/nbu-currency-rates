jQuery(document).ready(function () {
    var saveButton = jQuery('#save');

    restoreOptions();
    saveButton.click(function () {
        saveOptions();
    });
});

function saveOptions() {
    var currency = jQuery('#currency').val();
    chrome.storage.sync.set({
        'currency': currency
    }, function () {
        var statusEl = jQuery('#status');
        statusEl.html('Options saved').show();
        setTimeout(function () {
            statusEl.fadeOut();
        }, 1000);
        updateBadge();
    });
}

function restoreOptions() {
    chrome.storage.sync.get({
        'currency': 'USD'
    }, function (data) {
        jQuery('#currency').val(data.currency);
    });
}

function updateBadge() {
    chrome.storage.sync.get({
        'currency': 'USD'
    }, function (data) {
        chrome.browserAction.setBadgeText({'text': '---'});
        var date = new currentDate();
        date = date.getDate();
        var currency = data.currency;
        var url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=' + currency + '&date=' + date + '&json';

        jQuery.ajax({
            url: url,
            success: function (result) {
                if (result[0] && result[0]['rate']) {
                    var rate = result[0]['rate'];
                    rate = Math.round(rate * 100) / 100;
                    chrome.browserAction.setBadgeText({'text': rate.toString()});
                }
            }
        });
    });
}

function currentDate() {
    var currentDate, today = new Date(), year = today.getFullYear(), month = today.getMonth() + 1, day = today.getDate();
    month = month < 10 ? '0' + month : month;
    day = day < 10 ? '0' + day : day;
    this.year = year;
    this.month = month;
    this.day = day;
    this.getDate = function () {
        return this.year.toString() + this.month.toString() + this.day.toString();
    }
}
