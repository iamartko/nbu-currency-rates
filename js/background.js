chrome.browserAction.setBadgeBackgroundColor({'color': '#146731'});
displayBadge();

function displayBadge() {
    chrome.storage.sync.get({
        'currency': 'USD'
    }, function (data) {
        chrome.browserAction.setBadgeText({'text': '---'});
        var date = new currentDate();
        date = date.getDate();
        var currency = data.currency;
        var url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=' + currency + '&date=' + date + '&json';

        jQuery.ajax({
            url: url,
            success: function (result) {
                if (result[0] && result[0]['rate']) {
                    var rate = result[0]['rate'];
                    rate = Math.round(rate * 100) / 100;
                    chrome.browserAction.setBadgeText({'text': rate.toString()});
                }
            }
        });
    });
    setTimeout(displayBadge, 3600000);
}

function currentDate() {
    var today = new Date(), year = today.getFullYear(), month = today.getMonth() + 1, day = today.getDate();
    month = month < 10 ? '0' + month : month;
    day = day < 10 ? '0' + day : day;
    this.year = year;
    this.month = month;
    this.day = day;
    this.getDate = function () {
        return this.year.toString() + this.month.toString() + this.day.toString();
    }
}
